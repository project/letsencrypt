<?php

namespace Drupal\letsencrypt\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\idna\Service\IdnaConvert;
use LEClient\LEClient;
use LEClient\LEOrder;

/**
 * Class Lets Encrypt.
 */
class LetsEncrypt {

  //phpcs:disable
  private ConfigFactoryInterface $configFactory;
  private IdnaConvert $idna;
  private LEClient $client;
  private LEOrder $order;
  private string $type;
  private string $mode;
  private string $email;
  private string $logLevel;
  private string $basename;
  private string $cdir;
  private string $dirname;
  private array $acc;
  private array $extra;
  private string $challenge;
  private array $domains;
  private $logger = NULL;
  //phpcs:enable

  /**
   * Creates a new Lescript.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
      IdnaConvert $idna) {
    // Services.
    $config = $config_factory->get('letsencrypt.settings');
    $this->configFactory = $config_factory;
    $this->idna = $idna;
    // Configs.
    $this->type = LEOrder::CHALLENGE_TYPE_HTTP;
    $this->mode = $config->get('acme-url');
    $this->email = $config->get('cert-email');
    $this->logLevel = $config->get('acme-log');
    $this->basename = FALSE;
    // Cert & Accaunt Dir.
    $cdir = $this->prepareDir($config);
    $this->cdir = $cdir;
    $this->acc = [
      "private_key" => "$cdir/_account/private.pem",
      "public_key" => "$cdir/_account/public.pem",
    ];
    // Challenge.
    $this->challenge = DRUPAL_ROOT . '/.well-known/acme-challenge';
  }

  /**
   * Init.
   */
  public function init($type = LEOrder::CHALLENGE_TYPE_HTTP) : LetsEncrypt {
    $this->type = $type;
    return $this;
  }

  /**
   * Mode: stage/prod.
   */
  public function setMode(string $mode = 'stage') : LetsEncrypt {
    $this->mode = $mode;
    return $this;
  }

  /**
   * Get Mode URL: stage/prod.
   */
  public function getModeUrl() : string {
    $url = [
      'prod' => 'https://acme-v02.api.letsencrypt.org',
      'stage' => 'https://acme-staging-v02.api.letsencrypt.org',
    ];
    return $url[$this->mode];
  }

  /**
   * Mode: stage/prod.
   */
  public function setLogLevel(string $level = 'LOG_OFF') : LetsEncrypt {
    $this->logLevel = $level;
    return $this;
  }

  /**
   * Get Mode URL: stage/prod.
   */
  public function getLogLevel() : int {
    // Configs.
    $log = [
      // Logs no messages or faults, except Runtime Exceptions.
      'LOG_OFF' => 0,
      // Logs only messages and faults.
      'LOG_STATUS' => 1,
      // Logs messages, faults and raw responses from HTTP requests.
      'LOG_DEBUG' => 2,
    ];
    return $log[$this->logLevel];
  }

  /**
   * SET certificate directory.
   */
  public function setCertDir(string $cdir) : LetsEncrypt {
    // $cflag = FileSystemInterface::CREATE_DIRECTORY;
    // \Drupal::service('file_system')->prepareDirectory($directory, $cflag); .
    $this->cdir = $cdir;
    $this->createDir("$cdir/_account");
    $this->acc = [
      "private_key" => "$cdir/_account/private.pem",
      "public_key" => "$cdir/_account/public.pem",
    ];
    return $this;
  }

  /**
   * SET Domains.
   */
  public function setExtra(array $extra) : LetsEncrypt {
    $this->extra = $extra;
    return $this;
  }

  /**
   * SET Domains.
   */
  public function setDomains(array $domains) : LetsEncrypt {
    $this->domains = array_map(function ($domain) {
      return \Drupal::service('idna')->encode($domain);
    }, $domains);
    if (!$this->basename) {
      $this->basename = reset($this->domains);
    }
    return $this;
  }

  /**
   * SET BaseName.
   */
  public function setBaseName(string $basename) : LetsEncrypt {
    $this->basename = $basename;
    return $this;
  }

  /**
   * SET DirName.
   */
  public function setDirName(string $dirname) : LetsEncrypt {
    $this->dirname = $dirname;
    return $this;
  }

  /**
   * Read Cert.
   */
  public function read(string $host) {
    $path = "{$this->cdir}/$host";
    return $this->info($path);
  }

  /**
   * Read Cert.
   */
  public function info($dir) {
    $cert = FALSE;
    $private = "{$dir}/private.pem";
    $fullchain = "{$dir}/fullchain.pem";
    if (file_exists($fullchain)) {
      $cert_read = openssl_x509_read("file://$fullchain");
      $cert_info = openssl_x509_parse($cert_read);
      if ($cert_info && $cert_info['validTo_time_t'] > time()) {
        $days = round(($cert_info['validTo_time_t'] - time()) / 3600 / 24);
        $expire = date('d.M.Y', $cert_info['validTo_time_t']);
        $date = date('Y-m-d\TH:i:s', $cert_info['validTo_time_t']);
        $alt = str_replace(["DNS:", " "], "", $cert_info['extensions']['subjectAltName']);
        // array_map(fn($domain) => $idna->decode($domain), $names)); //.
        $names = array_map(function ($domain) {
          return \Drupal::service('idna')->decode($domain);
        }, explode(",", $alt));
        $cert = [
          'date' => $date,
          'days' => $days,
          'names' => $names,
          'expire' => $expire,
          'message' => "<b>{$days}</b> days left, expire at {$expire}",
          'private' => file_get_contents($private),
          'fullchain' => file_get_contents($fullchain),
        ];
      }
    }
    return $cert;
  }

  /**
   * Sign.
   */
  public function signPipe(?\Closure $callback = NULL) : array {
    $output = [];
    $cert = $this->cert($this->dirname);
    $client = new LEClient(
      $this->email,
      $this->getModeUrl(),
      $this->getLogLevel(),
      $cert,
      $this->acc
    );
    $account = $client->getAccount();
    // Order.
    $this->log("signPipe:: Start 🚀");
    $order = $client->getOrCreateOrder($this->basename, $this->domains);
    // Check whether there are any authorizations pending.
    // If that is the case, try to verify the pending authorizations.
    $this->log("signPipe:: ORDER!! Step-1");
    if (!$order->allAuthorizationsValid()) {
      $this->log("signPipe:: order - NOT allAuthorizationsValid");
      // Get the HTTP challenges from the pending authorizations.
      $pending = $order->getPendingAuthorizations($this->type);
      $this->log("signPipe:: pending");
      // Do Acme Challenge.
      if (!empty($pending)) {
        $this->log("signPipe:: pending foreach ->>");
        foreach ($pending as $challenge) {
          // Wright challenge.
          $this->log("signPipe:: 🚀🚀🚀 {$challenge['identifier']}");
          if (!$callback) {
            $this->acmeChallenge($challenge);
          }
          else {
            $callback($challenge, $this->extra);
          }
          // Let LetsEncrypt verify this challenge.
          $verif = $order->verifyPendingOrderAuthorization($challenge['identifier'], $this->type, FALSE);
          $this->log("signPipe:: verify {$challenge['identifier']} ->> verify [$verif]");
          if ($verif) {
            $this->log("signPipe:: ⭐️⭐️⭐️ verify {$challenge['identifier']} ->> Success");
          }
          else {
            $this->log("signPipe:: 🔴🔴🔴 verify {$challenge['identifier']} ->> FAIL");
          }
        }
      }
    }
    // Check once more whether all authorizations are valid
    // before we can finalize the order.
    $this->log("signPipe:: ORDER!! Step-2");
    if ($order->allAuthorizationsValid()) {
      $this->log("signPipe:: order - allAuthorizationsValid");
      // Finalize the order first, if that is not yet done.
      if (!$order->isFinalized()) {
        $this->log("signPipe:: order - finalizeOrder");
        $order->finalizeOrder();
      }
      // Check whether the order has been finalized before we can get
      // the certificate. If finalized, get the certificate.
      if ($order->isFinalized()) {
        $this->log("signPipe:: order - isFinalized");
        $order->getCertificate();
        file_put_contents($cert['domains'], implode("\n", $this->domains));
        $date = time() + 60 * 60 * 24 * 90;
        $datetime = \Drupal::service('date.formatter')->format($date, 'custom', 'Y-m-d\TH:i:s');
        file_put_contents($cert['expire'], $datetime);
        $this->log("signPipe:: ✅✅✅ Save cert to disc");
      }
      else {
        $this->log("signPipe:: 🔥 FAIL order - isFinalized");
      }
    }
    else {
      $this->log("signPipe:: 🔥 INVALID Authorization");
      $pending = $order->getPendingAuthorizations($this->type);
      if (is_array($pending)) {
        foreach ($pending as $challenge) {
          $file = "{$challenge['identifier']}/.well-known/acme-challenge/{$challenge['filename']}";
          $this->log("signPipe:: 🔥 FAIL {$challenge['identifier']}");
          $this->log("signPipe:: File: {$file}");
          $this->log("signPipe:: Content: {$challenge['content']}");
        }
      }
      else {
        $invlid = $order->getAuthorizations($this->type, 'invalid', 'invalid');
        foreach ($invlid as $challenge) {
          $file = "{$challenge['identifier']}/.well-known/acme-challenge/{$challenge['filename']}";
          $this->log("signPipe:: 🔥 FAIL {$challenge['identifier']}");
          $this->log("signPipe:: File: {$file}");
          $this->log("signPipe:: Content: {$challenge['content']}");
        }
        $this->log("signPipe:: 🔥 FAIL & pending is blank 😱");
      }
    }
    $output['domains'] = implode(", \n", $this->domains);
    return $output;
  }

  /**
   * Get Cert.
   */
  public function sign($basename, array $domains = []) : string {
    $all = [];
    $this->domains = array_map(function ($domain) {
      $domain = trim($domain);
      return \Drupal::service('idna')->encode($domain);
    }, $domains);
    $this->basename = $basename;
    $this->dirname = $basename;
    // $callback = $this->acmeChallenge();
    $this->signPipe();
    return implode(", \n", $this->domains);
  }

  /**
   * Acme Challenge.
   */
  private function acmeChallenge($challenge, $drupalroot = TRUE) : void {
    \Drupal::service('module_handler')->alter('letsencrypt_challenge', $challenge, $drupalroot);
    if ($drupalroot) {
      $dir = $this->challenge;
      $this->createDir($this->challenge, FALSE);
      file_put_contents("{$dir}/{$challenge['filename']}", $challenge['content']);
    }
  }

  /**
   * Prepare cert directory.
   */
  private function prepareDir($config) : string {
    $cdir = getenv("HOME") . "cert";
    if ($dir = $config->get('cert-dir')) {
      if (substr($dir, 0, '2') == '~/') {
        $dir = getenv("HOME") . substr($dir, 2);
      }
      if (substr($dir, 0, '2') == 'private://') {
        $dir = \Drupal::service('file_system')->realpath($dir);
      }
      $cdir = $dir;
    }
    $this->createDir($cdir);
    $this->createDir("$cdir/_account");
    return $cdir;
  }

  /**
   * Cert Path.
   */
  public function cert(string $base) : array {
    $ddir = \Drupal::transliteration()->transliterate("{$this->cdir}/$base");
    $this->createDir($ddir);
    $cert = [
      "public_key" => "{$ddir}/public.pem",
      "private_key" => "{$ddir}/private.pem",
      "certificate" => "{$ddir}/certificate.crt",
      "fullchain_certificate" => "{$ddir}/fullchain.pem",
      "order" => "{$ddir}/order",
      "domains" => "{$ddir}/domains",
      "expire" => "{$ddir}/expire",
    ];
    return $cert;
  }

  /**
   * Create Directory.
   */
  private function createDir(string $directory, bool $protect = TRUE) {
    if (!file_exists($directory)) {
      mkdir($directory, 0777, TRUE);
      if ($protect) {
        file_put_contents("$directory/.htaccess", "order deny,allow\ndeny from all");
      }
    }
  }

  /**
   * Set Logger.
   */
  public function setLogger($logger) : LetsEncrypt {
    $this->logger = $logger;
    return $this;
  }

  /**
   * Log.
   */
  private function log(string $message) : void {
    if ($this->logger) {
      $this->logger->logMessage($message);
    }
  }

}
