# LetsEncrypt

https://www.drupal.org/project/letsencrypt

## PHP LetsEncrypt client library integration.
https://github.com/yourivw/LEClient

PHP LetsEncrypt client library for ACME v2. The aim of this client is to make an easy-to-use and integrated solution to create a LetsEncrypt-issued SSL/TLS certificate with PHP. The user has to have access to the web server or DNS management to be able to verify the domain is accessible/owned by the user.

## NB!
Do not forget to remove `_account` when change acme-url `satage`/`prod`

## CODE usage:
```php
<?php
$domain = "example.org";
\Drupal::service('letsencrypt')->sign($domain);
\Drupal::service('letsencrypt')->read($domain);
```
### Custom callback
```php
<?php
$callback = function ($challenge, array $extra = []) {
  $acme = "{$extra['webroot']}/.well-known/acme-challenge";
  $commands = [
    "mkdir -p $acme",
    "\"echo '{$challenge['content']}' > $acme/{$challenge['filename']}\"",
  ];
  // Do some shell_exec();
};
$domain = [
  'certdir' => '/var/www/private',
  'domains' => [
    'example.com',
    'www.example.com',
    'example.org',
    'www.example.org',
  ],
  'name' => 'example.com',
  'webroot' => '/var/www/html',
];
$le = \Drupal::service('letsencrypt')
  ->init()
  ->setMode('prod')
  ->setCertDir($domain['certdir'])
  ->setDomains($domain['domains'])
  ->setDirName($domain['name'])
  ->setExtra([
    'webroot' => $domain['webroot'],
  ]);
$le->signPipe($callback);
```
### Wildcard SSL with DNS callback and AWS Route53
AWSPolicy:
```js
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Action": [
        "route53:GetChange",
        "route53:ChangeResourceRecordSets",
        "route53:ListResourceRecordSets"
      ],
      "Resource": [
        "arn:aws:route53:::hostedzone/*",
        "arn:aws:route53:::change/*"
      ]
    },
    {
      "Sid": "",
      "Effect": "Allow",
      "Action": "route53:ListHostedZonesByName",
      "Resource": "*"
    }
  ]
}
```
Drupal code:
```php
<?php
use LEClient\LEOrder;
use Aws\Route53\Route53Client;
$dns_callback = function ($challenge, array $extra = []) {
  $domain = "_acme-challenge.{$challenge['identifier']}";
  $hardcoded_domain = '_acme-challenge.example.com.';
  $client = Route53Client::factory([
    'credentials' => [
      'key' => '***AWS***KEY***',
      'secret' => '***AWS***SECRET***',
    ],
    'region'  => '***AWS***REQION***EXAMPLE***eu-west-1***',
    'version' => 'latest',
  ]);
  $record = [
    ['Value' => "\"{$challenge['DNSDigest']}\""],
  ];
  $result = $client->changeResourceRecordSets([
    'HostedZoneId' => '***AWS***Route53***ZONE**ID***',
    'ChangeBatch' => [
      'Comment' => 'Acme challenge',
      'Changes' => [
        [
          // 'Action' => 'CREATE', It's Better to create Manually.
          'Action' => 'UPSERT',
          'ResourceRecordSet' => [
            'TTL' => 600,
            'Type' => 'TXT',
            'Name' => $hardcoded_domain,
            'ResourceRecords' => $record,
          ],
        ],
      ],
    ],
  ]);
  return $result;
};

// Exec.
$domain = [
  'certdir' => '/var/www/private',
  'domains' => [
    'example.com',
    '*.example.com',
  ],
  'name' => 'example.com',
];
$le = \Drupal::service('letsencrypt')
  ->init(LEOrder::CHALLENGE_TYPE_DNS)
  ->setCertDir($domain['certdir'])
  ->setDomains($domain['domains'])
  ->setDirName($domain['name'])
  ->setExtra([]);
$le->signPipe($dns_callback);
```
